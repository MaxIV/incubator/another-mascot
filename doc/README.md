# Mascot

*Also called Tango 10 - prototype*

## Main goals

* Tango without Corba ...
  - ... with backwards compatibility of existing user code
* Write a Tango Specification (at reasonable level / depth)
* Demonstrate ways to modularise the Tango ecosystem

In order for this prototype to be adopted into something more, it has to keep compatibility with Tango. 

The demo that is described below will try to incorporate important aspects of this, by using Taurus and code that works with Tango. 

## Technical requirements

* Network friendly for modern network setups
 - Dockeriseable
* Language agnostic
 - The idea is to rely on communication protocol and specification instead of a programming language
* RPC
* Events / notification system
 - Pub / sub?
* Service discovery
 - Name
 - Location
* Persistance
 - Config
 - Properties
* One object per equipment
* A `Mascot` is either connected to one equipment or composed of other `Mascots`

### For the demo

Base on gRPC and ZeroMQ. Replacing *only* CORBA wil be really difficult, because it's very ingrained in the Tango C++ core. So, in order to make a demo, we're replacing the Tango API with an identical API, that instead of talking with the underlying C++ / CORBA layer, it talks Mascot. 

* A Registry service for discovery
* A DeviceServer on top of Mascot
* A DeviceClient on top of Mascot

## Demo

* A Device Server
  - That serves one device (`mascot/test/1`) with one boolean attribute: `On`
* A Taurus GUI with a box that is either RED or GREEN
* The GUI is a Tango10/Mascot client that subscribes to the device on the Device Server
* A client that can send a write attribute request to change the value of `On` and hence update the GUI

### How to and where to start

The goal is to make a PROTOTYPE! It does not have to be perfect, we are aiming for a proof of concept, to run Tango on top of gRPC and ZeroMQ instead of CORBA. 

The way we aim to do it, to keep backwards compatibility is to implement a Tango Device Server, with the requirements specified for the demo. When that's done and works we basically swap out the `tango ` library with the `mascot ` library without any user code changes. 

#### Sample folder setup

```
lib-maxiv-mascot           // PYTHONPATH set to this directory
├── demo
│   └── MascotTest.py      // got the line "import tango" and "from tango.server import Device"
├── tango                  // dummy tango library
│   ├── server.py          // dummy file that imports Mascot "representatives" to solve the problem
│   ├── ...
│   ├── ...
│   └── ...
└── mascot
    ├── registry.py
    ├── device_server.py
    ├── client.py
    ├── ...
    ├── ...
    └── ...
```

## Architecture

For a quick overview please see the sketch in `doc/flow.jpg`

### Registry

The registry is a central service that holds a list of devices in the system, and which server each device is served on. When a server starts up it should register itself in the registry, hence, the adress to the registry must be known on startup time of the server. When a client wants to talk to a device, it should ask the registry for the adress of the server (that serves the device), with a device name as parameter, and will then connect to the server given. 

When a device is registered, the Registry should try to call the server for the device and ask it to serve this new device. 

*Sample communictaion interface (pseudo code)*

```
RegisterDevice(name, server_name)          // called by some tool to create a device in the system
RegisterServer(address, port)              // called by Server on startup
LocateDevice(name) address, port           // called by Client
```

### Mascot Device Server (Server)

The server is the process that serves devices in the network. 

*Sample communication interface (pseudo code)*

```
// Called by clients
ReadAttribute(device, attribute_name) value
WriteAttribute(device, attribute_name, value) error?
ExecuteCommand(device, command_name, params) error?

Subscribe(device, attribute_name)
Unsubscribe(device, attribute_name)

// Called by registry
ServeDevices(device_list)
```

### Mascot Device Client (Client)

The client that talks to the registry and a device server. The client is only a consumer of the `Registry` and the `Server` and does not have an (network / communicaton) interface by itself. 


#### C++

Currently a C++ implementation of a Mascot device is being developed. This can be found in the cpp/ dir. 

##### docker

It uses a Dockerfile that builds Centos7, protoc and gRPC. 

To run the docker first it needs to be built:
+ Navigate to the root of the project,
```
$ docker build -t lib-maxiv-mascot-cpp cpp/docker
$ docker-compose up
```
The container is now up and running. To access it, either type:
```
$ docker exec -it lib-maxiv-mascot /bin/bash
```
or 
```
$ ssh -p 14534 root@localhost
```
to access it through ssh. The password is 'root'.

##### cmake

The project builds using cmake since it involves much less toiling. Cmake reads a CMakeLists.txt and from it generates a Makefile. The CMakeLists.txt is pretty self-explanatory.

To build the project without having changed the CMakeLists.txt simply issue make.

If changes to the CMakeLists.txt has been make you will, from inside cpp/, need to type:
```
$ cmake3 .
$ make
```

##### running

To try out the DeviceProxy make sure to run two terminals with 
1. run `docker-compose up` from the root of the project.
2. Open a terminal and run
```
  $ docker exec -it lib-maxiv-mascot-py /bin/bash
  $ cd root/workspace/python
  $ python DatabaseDS.py
```
3. Open another terminal and run
```
  $ docker exec -it lib-maxiv-mascot-py /bin/bash
  $ cd root/workspace/python
  $ TANGO_HOST=localhost:50000 python demo.py D. The 'D' was chosen arbitrarily. It is just a name for the server and doesn't do anything.
```
4. Open one more terminal and run the following commands
```
  $ docker exec -it lib-maxiv-mascot-cpp /bin/bash
  $ cd root/workspace/cpp
  $ cmake3 .
  $ make
  $ ./bin/DeviceProxy
```
5. Now you should be able to read attributes from the python device using the DeviceProxy that was just launched by typing an attribute name.
Some attributes to try out:
 + ampli
 + float_attr
 + bool_attr
