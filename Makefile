gogen: goclean
	protoc -I proto \
	proto/* \
	--go_out=plugins=grpc:gen/gomascot

goclean:
	rm -f gen/gomascot/*.go

pygen: pyclean
	protoc -I proto \
	proto/* \
	--python_out=plugins=grpc:gen/pymascot

pyclean:
	rm -f gen/pymascot/*.py

gorunserver:
	go run golang/server.go

gorunclient:
	go run golang/client.go
