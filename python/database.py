from pymascot.database_pb2_grpc import DirectoryStub
from pymascot.database_pb2 import DbDevExportInfo, DeviceName
import grpc
import os


class DatabaseClient:
    def __init__(self, database_host=None):
        if not database_host:
            database_host = os.getenv["TANGO_HOST"]

        self.host = database_host
        self.channel = grpc.insecure_channel(self.host)
        self.directory = DirectoryStub(self.channel)

    def export_device(self, device_name, host, port):
        dev_info = DbDevExportInfo()
        dev_info.name = device_name
        dev_info.address = "{}:{}".format(host, port)
        dev_info.host = host
        dev_info.version = "345"
        dev_info.pid = os.getpid()
        reply = self.directory.ExportDevice(dev_info)
        return reply

    def import_device(self, device_name):
        ds_name = DeviceName(name=device_name)
        return self.directory.ImportDevice(ds_name)


Database = DatabaseClient


# db = DatabaseClient("localhost:50000")
# db.export_device("test/antdup/1", "localhost", "50000")
# db.import_device("test/antdup/1")
