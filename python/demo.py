from server import Device, attribute
from tango import AttrQuality
from time import time


class TestDevice(Device):
    ampli = attribute(dtype=int)
    bool_attr = attribute(dtype=bool)
    float_attr = attribute(dtype=float)
    string_attr = attribute(dtype=str)
    error = attribute(dtype=bool, fget="raise_error")

    def read_ampli(self):
        return 42

    def read_bool_attr(self):
        return True, AttrQuality.ATTR_INVALID

    def read_float_attr(self):
        return 34.5, 3550234237.123

    def read_string_attr(self):
        return ("Hello World", AttrQuality.ATTR_INVALID, time())

    def write_ampli(self, value):
        print("Write ampli {}".format(value))

    def raise_error(self):
        raise ValueError("Some Error")


TestDevice.run_server()
