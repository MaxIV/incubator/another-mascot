# Protocol buffer imports
from gen_grpc.device_proxy_pb2 import (
    DeviceAttribute,
    AttributeDimension,
    TimeVal,
)
from gen_grpc.device_proxy_pb2 import RPCReply, DeviceAttributeMessage, CommandReplyMessage, CommandReply

# GRPC imports
import grpc
from gen_grpc.device_proxy_pb2_grpc import DeviceProxyServicer
from gen_grpc.device_proxy_pb2_grpc import add_DeviceProxyServicer_to_server

# Tango
import tango

# Time
from time import sleep

# Gevent
USE_GEVENT = False
if USE_GEVENT:
    from tango.gevent import DeviceProxy
    from gevent.queue import Queue
    from gevent.threadpool import ThreadPoolExecutor
else:
    from tango import DeviceProxy
    from queue import Queue
    from concurrent.futures import ThreadPoolExecutor

# Logger
import logging

logger = logging.getLogger("gateway")
logger.setLevel(logging.DEBUG)

type_binding = {tango.ArgType.DevDouble: float}


def tango_attribute_to_grpc(attr):
    """ Build a GRPC DeviceAttribute reply """
    reply = DeviceAttribute()
    # Device name
    reply.name = attr.name
    # Data format is a AttrDataFormat
    reply.data_format = int(attr.data_format)
    # Device type is an CmdArgType
    reply.type = int(attr.type)
    # Attribute x and y dimension
    reply.dim_x = attr.dim_x
    reply.dim_y = attr.dim_y
    r_dim = reply.r_dimension.add()
    r_dim.dim_x = attr.r_dimension.dim_x
    r_dim.dim_y = attr.r_dimension.dim_y
    # Attribute write dimension
    reply.w_dim_x = attr.w_dim_x
    reply.w_dim_y = attr.w_dim_y
    w_dim = reply.w_dimension.add()
    w_dim.dim_x = attr.w_dimension.dim_x
    w_dim.dim_y = attr.w_dimension.dim_y
    # Read request statut
    # TODO: Maybe use those info to feed the DeviceAttributeMessage
    reply.has_failed = attr.has_failed
    reply.is_empty = attr.is_empty
    # ???
    reply.nb_read = attr.nb_read
    reply.nb_written = attr.nb_written
    # Read and write values (set a str, shall be move to bytes)
    reply.value = str(attr.value)
    reply.w_value = str(attr.w_value)
    # Device quality is an AttrQuality
    reply.quality = int(attr.quality)
    # Device timestamp
    _time = reply.time.add()
    _time.tv_sec = attr.time.tv_sec
    _time.tv_nsec = attr.time.tv_nsec
    _time.tv_usec = attr.time.tv_usec
    return reply


class DeviceServicer(DeviceProxyServicer):
    """ The GRPC service """

    def read_attribute(self, request, context):
        logger.info("Enter read attribute")
        debug = "read_attribute:\n - request: {}\n - context: {}"
        logger.debug(debug.format(request, context))
        device_name = request.device
        attribute = request.attribute
        if USE_GEVENT:
            device_proxy = DeviceProxy(device_name, wait=True)
            tango_reply = device_proxy.read_attribute(attribute, wait=True)
        else:
            device_proxy = DeviceProxy(device_name)
            tango_reply = device_proxy.read_attribute(attribute)
        debug = "Attribute reply for {}:{} :: {}"
        logger.debug(debug.format(device_proxy, attribute, tango_reply))
        dev_attr = tango_attribute_to_grpc(tango_reply)
        return DeviceAttributeMessage(data=dev_attr, success=True)

    def write_attribute(self, request, context):
        logger.info("Enter write attribute")
        reply = RPCReply(success=True)
        device_name = request.device
        attribute = request.attribute
        value = request.value
        try:
            if USE_GEVENT:
                device_proxy = DeviceProxy(device_name, wait=True)
                attr_type = device_proxy.get_attribute_config(
                    attribute
                ).data_type
                typed_value = type_binding[attr_type](value)
                device_proxy.write_attribute(attribute, typed_value, wait=True)
            else:
                device_proxy = DeviceProxy(device_name)
                attr_type = device_proxy.get_attribute_config(
                    attribute
                ).data_type
                typed_value = type_binding[attr_type](value)
                device_proxy.write_attribute(attribute, typed_value)
        except tango.DevFailed as e:
            logger.error("Error during tango calls: {}".format(e))
            reply.success = False
        return reply

    def command_inout(self, request, context):
        logger.info("Enter command inout {} {}".format(request, context))
        device_name = request.device
        command = request.command
        arg = request.arg
        device_proxy = DeviceProxy(device_name)
        info = device_proxy.get_command_config(command)
        if info.in_type:
            arg = type_binding[info.in_type(arg)]
            output = device_proxy.command_inout(command, arg)
        else:
            output = device_proxy.command_inout(command)
        if info.out_type:
            data = CommandReply(type=int(info.out_type), value=str(output))
            reply = CommandReplyMessage(data=data, success=True)
        else:
            reply = CommandReplyMessage(success=True)
        return reply

    def subscribe_to(self, request, context):
        logger.info("subscribe to: {}, {}".format(request, context))
        _queue = Queue()
        device = request.device
        evt_type = request.event_type
        attr = request.attribute

        def on_event(event):
            logger.debug("Get event {}".format(event))
            _queue.put(event)

        proxy = DeviceProxy(device)
        evt_id = proxy.subscribe_event(
            attr, tango.EventType.PERIODIC_EVENT, on_event
        )
        while True:
            event = _queue.get()
            logger.debug("Get event")
            reply = tango_attribute_to_grpc(event.attr_value)
            logger.debug("Sending message")
            yield DeviceAttributeMessage(success=True, data=reply)
            logger.debug("Message sent")

        logger.info("Unsubscribing {} {}".format(proxy, evt_id))
        proxy.unsubscribe_event(evt_id)
        # Bad implementation no unsubscribe


def run_server(port):
    server = grpc.server(ThreadPoolExecutor(max_workers=10))
    device_service = DeviceServicer()
    add_DeviceProxyServicer_to_server(device_service, server)
    logger.info("Starting server. Listening on port {}.".format(port))
    server.add_insecure_port("[::]:{}".format(port))
    server.start()
    try:
        while True:
            sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    run_server(50000)
