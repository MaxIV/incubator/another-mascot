import grpc

# import the generated classes
from pymascot.attributes_pb2 import ReadAttributeMessage
from pymascot.device_server_pb2_grpc import MascotDeviceServerStub as DeviceProxy


channel = grpc.insecure_channel('localhost:50001')
proxy = DeviceProxy(channel)

# Read attributeName
print("Read attributeName: ")
attr = ReadAttributeMessage(deviceName="sys/tg_test/1", attributeName="ampli")
response = proxy.ReadAttribute(attr)
print(response)

# Read attributeName bool
print("Read attributeName bool: ")
attr = ReadAttributeMessage(deviceName="sys/tg_test/1", attributeName="bool_attr")
response = proxy.ReadAttribute(attr)
print(response)
print(response.success)


# Read attributeNamei float
print("Read attributeName float attr: ")
attr = ReadAttributeMessage(deviceName="sys/tg_test/1", attributeName="float_attr")
response = proxy.ReadAttribute(attr)
print(response)
print(response.success)


# Read attributeNamei strin
print("Read attributeName string: ")
attr = ReadAttributeMessage(deviceName="sys/tg_test/1", attributeName="string_attr")
response = proxy.ReadAttribute(attr)
print(response)
print(response.success)


# Read attributeNamei Error
print("Read attributeName Error: ")
attr = ReadAttributeMessage(deviceName="sys/tg_test/1", attributeName="error")
response = proxy.ReadAttribute(attr)
print(response)
print(response.success)


#
# # Write attributeName
# print("Write attributeName")
# w_attr = deviceName_proxy_pb2.WriteattributeNameName(device="sys/tg_test/1", attribute="ampli", value="34")
# response = proxy.write_attributeName(w_attr)
# print(response)
#
#
# # Command
# print("Get command")
# cmd = deviceName_proxy_pb2.CommandName(device="sys/tg_test/1", command="Switchstates")
# response = proxy.command_inout(cmd)
# print(response)
#
# print(enums_pb2.EventType)
# # Subscribe event
# sub = deviceName_proxy_pb2.Subscription(device="sys/tg_test/1", attributeName="ampli", event_type=2)
# stream = proxy.subscribe_to(sub)
#
# loop = 0
# for evt in stream:
#     print("New event")
#     print(evt)
#     loop += 1
#     if loop == 3:
#        break
