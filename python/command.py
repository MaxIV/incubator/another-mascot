import functools


def command(
    f=None,
    dtype_in=None,
    dformat_in=None,
    doc_in="",
    dtype_out=None,
    dformat_out=None,
    doc_out="",
    display_level=None,
    polling_period=None,
    green_mode=None,
):
    print("f", f)
    if f is None:
        return functools.partial(
            command,
            dtype_in=dtype_in,
            dformat_in=dformat_in,
            doc_in=doc_in,
            dtype_out=dtype_out,
            dformat_out=dformat_out,
            doc_out=doc_out,
            display_level=display_level,
            polling_period=polling_period,
            green_mode=green_mode,
        )
    else:

        @functools.wraps(f)
        def cmd(self, *args, **kwargs):
            print(f)
            return f(self, *args, **kwargs)

        return cmd
