from enum import Enum
from pymascot import tango_enums_pb2


class AttrQuality(Enum):
    ATTR_VALID = tango_enums_pb2.ATTR_VALID
    ATTR_INVALID = tango_enums_pb2.ATTR_INVALID
    ATTR_ALARM = tango_enums_pb2.ATTR_ALARM
    ATTR_CHANGING = tango_enums_pb2.ATTR_CHANGING
    ATTR_WARNING = tango_enums_pb2.ATTR_WARNING


class DevState(Enum):
    ON = tango_enums_pb2.ON
    OFF = tango_enums_pb2.OFF
    CLOSE = tango_enums_pb2.CLOSE
    OPEN = tango_enums_pb2.OPEN
    INSERT = tango_enums_pb2.INSERT
    EXTRACT = tango_enums_pb2.EXTRACT
    MOVING = tango_enums_pb2.MOVING
    STANDBY = tango_enums_pb2.STANDBY
    FAULT = tango_enums_pb2.FAULT
    INIT = tango_enums_pb2.INIT
    RUNNING = tango_enums_pb2.RUNNING
    ALARM = tango_enums_pb2.ALARM
    DISABLE = tango_enums_pb2.DISABLE
    UNKNOWN = tango_enums_pb2.UNKNOWN


class CmdArgType(Enum):
    DevVoid = tango_enums_pb2.DevVoid
    DevBoolean = tango_enums_pb2.DevBoolean
    DevShort = tango_enums_pb2.DevShort
    DevLong = tango_enums_pb2.DevLong
    DevFloat = tango_enums_pb2.DevFloat
    DevDouble = tango_enums_pb2.DevDouble
    DevUShort = tango_enums_pb2.DevUShort
    DevULong = tango_enums_pb2.DevULong
    DevString = tango_enums_pb2.DevString
    DevVarCharArray = tango_enums_pb2.DevVarCharArray
    DevVarShortArray = tango_enums_pb2.DevVarShortArray
    DevVarLongArray = tango_enums_pb2.DevVarLongArray
    DevVarFloatArray = tango_enums_pb2.DevVarFloatArray
    DevVarDoubleArray = tango_enums_pb2.DevVarDoubleArray
    DevVarUShortArray = tango_enums_pb2.DevVarUShortArray
    DevVarULongArray = tango_enums_pb2.DevVarULongArray
    DevVarStringArray = tango_enums_pb2.DevVarStringArray
    DevVarLongStringArray = tango_enums_pb2.DevVarLongStringArray
    DevVarDoubleStringArray = tango_enums_pb2.DevVarDoubleStringArray
    DevState = tango_enums_pb2.DevStates
    ConstDevString = tango_enums_pb2.ConstDevString
    DevVarBooleanArray = tango_enums_pb2.DevVarBooleanArray
    DevUChar = tango_enums_pb2.DevUChar
    DevLong64 = tango_enums_pb2.DevLong64
    DevULong64 = tango_enums_pb2.DevULong64
    DevVarLong64Array = tango_enums_pb2.DevVarLong64Array
    DevVarULong64Array = tango_enums_pb2.DevVarULong64Array
    DevInt = tango_enums_pb2.DevInt
    DevEncoded = tango_enums_pb2.DevEncoded
    DevEnum = tango_enums_pb2.DevEnum
    DevPipeBlob = tango_enums_pb2.DevPipeBlob


class AttrWriteType(Enum):
    READ = tango_enums_pb2.READ
    READ_WITH_WRITE = tango_enums_pb2.READ_WITH_WRITE
    WRITE = tango_enums_pb2.WRITE
    READ_WRITE = tango_enums_pb2.READ_WRITE


class AttrDataFormat(Enum):
    SCALAR = tango_enums_pb2.SCALAR
    SPECTRUM = tango_enums_pb2.SPECTRUM
    IMAGE = tango_enums_pb2.IMAGE
    FMT_UNKNOWN = tango_enums_pb2.FMT_UNKNOWN


class DispLevel(Enum):
  OPERATOR = tango_enums_pb2.OPERATOR
  EXPERT = tango_enums_pb2.EXPERT
