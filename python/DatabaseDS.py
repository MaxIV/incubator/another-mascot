from pymascot.database_pb2 import DeviceName, DbDevExportInfo, DbDevExportInfos
from pymascot.database_pb2 import DirectoryReply, DbDevImportInfo
from pymascot.database_pb2 import DirectoryImportReply
from pymascot.error_pb2 import Error
from pymascot.database_pb2_grpc import add_DirectoryServicer_to_server
from pymascot.database_pb2_grpc import DirectoryServicer
import grpc
from concurrent import futures
import logging
from time import sleep


class Directory(DirectoryServicer):
    device_map = {}

    def ExportDevice(self, request, context):
        self.device_map[request.name.lower()] = request
        reply = DirectoryReply()
        reply.success = True
        reply.error.code = 1
        reply.error.message = ""
        return reply

    def ExportDevices(self, request, context):
        pass

    def ImportDevice(self, request, context):
        reply = DirectoryImportReply()
        name = request.name
        try:
            dev = self.device_map[name.lower()]
        except KeyError:
            reply.error.code = 0
            reply.error.message = "Undifined device {}".format(name)
            reply.success = False
            return reply
        reply.info.name = name
        reply.info.address = dev.address
        return reply


#
# dev_info.name = "Hello world"
# dev_info.address = "localhost:50000"
# dev_info.exported =  False
# dev_info.version =  "version"
#

# create a gRPC server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
directory = Directory()
add_DirectoryServicer_to_server(directory, server)

logging.info("Starting server. Listening on port 50000.")
server.add_insecure_port("[::]:50000")
server.start()
logging.basicConfig(level=logging.INFO)
try:
    while True:
        sleep(86400)
except KeyboardInterrupt:
    server.stop(0)
