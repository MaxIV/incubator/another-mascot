import grpc

# import the generated classes
from pymascot.attributes_pb2 import ReadAttributeMessage
from pymascot.device_server_pb2_grpc import (
    MascotDeviceServerStub as GrpcDeviceProxy,
)
from errors import DevFailed
from database import Database
import os

class GrpcClient:
    def __init__(self, host_addr):
        self.host_addr = host_addr
        self.channel = grpc.insecure_channel(self.host_addr)
        self.proxy = GrpcDeviceProxy(self.channel)

    def read_attribute(self, device, attr):
        attr = ReadAttributeMessage(deviceName=device, attributeName=attr)
        response = self.proxy.ReadAttribute(attr)
        return response

    def read_attribute_config(self, device, attr):
        attr = ReadAttributeMessage(deviceName=device, attributeName=attr)
        response = self.proxy.ReadAttributeConfig(attr)
        return response



class DeviceProxy:
    def __init__(self, name, *args, **kwargs):
        self._name = name
        # Get info to from the database and store
        # NotImplemented
        self.__server_address = None
        self.__grpc_client = None

    @property
    def _server_address(self):
        if not self.__server_address:
            db = Database(os.getenv("TANGO_HOST"))
            reply = db.import_device(self._name)
            print('##')
            print(reply)
            print(reply.info)
            print(reply.info.address)
            print(type(reply.info.address))
            print('##')
            self.__server_address = reply.info.address
        return self.__server_address

    @property
    def _grpc_client(self):
        if not self.__grpc_client:
            self.__grpc_client = GrpcClient(self._server_address)
        return self.__grpc_client

    def add_logging_target(self, *args, **kwargs):
        raise NotImplementedError()

    def adm_name(self, *args, **kwargs):
        raise NotImplementedError()

    def alias(self, *args, **kwargs):
        raise NotImplementedError()

    def attribute_history(self, *args, **kwargs):
        raise NotImplementedError()

    def attribute_list_query(self, *args, **kwargs):
        raise NotImplementedError()

    def attribute_list_query_ex(self, *args, **kwargs):
        raise NotImplementedError()

    def attribute_query(self, *args, **kwargs):
        raise NotImplementedError()

    def black_box(self, *args, **kwargs):
        raise NotImplementedError()

    def cancel_all_polling_asynch_request(self, *args, **kwargs):
        raise NotImplementedError()

    def cancel_asynch_request(self, *args, **kwargs):
        raise NotImplementedError()

    def command_history(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout_raw(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout_reply_raw(self, *args, **kwargs):
        raise NotImplementedError()

    def command_list_query(self, *args, **kwargs):
        raise NotImplementedError()

    def command_query(self, *args, **kwargs):
        raise NotImplementedError()

    def connect(self, *args, **kwargs):
        raise NotImplementedError()

    def delete_property(self, *args, **kwargs):
        raise NotImplementedError()

    def description(self, *args, **kwargs):
        raise NotImplementedError()

    def dev_name(self, *args, **kwargs):
        raise NotImplementedError()

    def event_queue_size(self, *args, **kwargs):
        raise NotImplementedError()

    def get_access_control(self, *args, **kwargs):
        raise NotImplementedError()

    def get_access_right(self, *args, **kwargs):
        raise NotImplementedError()

    def get_asynch_replies(self, *args, **kwargs):
        raise NotImplementedError()

    def get_attribute_config(self, attr_name,  *args, **kwargs):
        attr = self._grpc_client.read_attribute_config(self._name, attr_name)
        if not attr.success:
            raise DevFailed(attr.error.message)
        return attr.data

    def get_attribute_config_ex(self, *args, **kwargs):
        raise NotImplementedError()

    def get_attribute_list(self, *args, **kwargs):
        raise NotImplementedError()

    def get_attribute_poll_period(self, *args, **kwargs):
        raise NotImplementedError()

    def get_command_config(self, *args, **kwargs):
        raise NotImplementedError()

    def get_command_list(self, *args, **kwargs):
        raise NotImplementedError()

    def get_command_poll_period(self, *args, **kwargs):
        raise NotImplementedError()

    def get_db_host(self, *args, **kwargs):
        raise NotImplementedError()

    def get_db_port(self, *args, **kwargs):
        raise NotImplementedError()

    def get_db_port_num(self, *args, **kwargs):
        raise NotImplementedError()

    def get_dev_host(self, *args, **kwargs):
        raise NotImplementedError()

    def get_dev_port(self, *args, **kwargs):
        raise NotImplementedError()

    def get_device_db(self, *args, **kwargs):
        raise NotImplementedError()

    def get_events(self, *args, **kwargs):
        raise NotImplementedError()

    def get_fqdn(self, *args, **kwargs):
        raise NotImplementedError()

    def get_from_env_var(self, *args, **kwargs):
        raise NotImplementedError()

    def get_green_mode(self, *args, **kwargs):
        raise NotImplementedError()

    def get_idl_version(self, *args, **kwargs):
        raise NotImplementedError()

    def get_last_event_date(self, *args, **kwargs):
        raise NotImplementedError()

    def get_locker(self, *args, **kwargs):
        raise NotImplementedError()

    def get_logging_level(self, *args, **kwargs):
        raise NotImplementedError()

    def get_logging_target(self, *args, **kwargs):
        raise NotImplementedError()

    def get_pipe_config(self, *args, **kwargs):
        raise NotImplementedError()

    def get_pipe_list(self, *args, **kwargs):
        raise NotImplementedError()

    def get_property(self, *args, **kwargs):
        raise NotImplementedError()

    def get_property_list(self, *args, **kwargs):
        raise NotImplementedError()

    def get_source(self, *args, **kwargs):
        raise NotImplementedError()

    def get_tango_lib_version(self, *args, **kwargs):
        raise NotImplementedError()

    def get_timeout_millis(self, *args, **kwargs):
        raise NotImplementedError()

    def get_transparency_reconnection(self, *args, **kwargs):
        raise NotImplementedError()

    def import_info(self, *args, **kwargs):
        raise NotImplementedError()

    def info(self, *args, **kwargs):
        raise NotImplementedError()

    def init(self, *args, **kwargs):
        raise NotImplementedError()

    def is_attribute_polled(self, *args, **kwargs):
        raise NotImplementedError()

    def is_command_polled(self, *args, **kwargs):
        raise NotImplementedError()

    def is_dbase_used(self, *args, **kwargs):
        raise NotImplementedError()

    def is_event_queue_empty(self, *args, **kwargs):
        raise NotImplementedError()

    def is_locked(self, *args, **kwargs):
        raise NotImplementedError()

    def is_locked_by_me(self, *args, **kwargs):
        raise NotImplementedError()

    def lock(self, *args, **kwargs):
        raise NotImplementedError()

    def locking_status(self, *args, **kwargs):
        raise NotImplementedError()

    def name(self, *args, **kwargs):
        raise NotImplementedError()

    def pending_asynch_call(self, *args, **kwargs):
        raise NotImplementedError()

    def ping(self, *args, **kwargs):
        raise NotImplementedError()

    def poll_attribute(self, *args, **kwargs):
        raise NotImplementedError()

    def poll_command(self, *args, **kwargs):
        raise NotImplementedError()

    def polling_status(self, *args, **kwargs):
        raise NotImplementedError()

    def put_property(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attribute(self, attr_name, *args, **kwargs):
        attr = self._grpc_client.read_attribute(self._name, attr_name)
        if not attr.success:
            raise DevFailed(attr.error.message)
        return attr.data


    def read_attribute_asynch(self,*args, **kwargs):
        raise NotImplementedError()

    def read_attribute_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attributes(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attributes_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attributes_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def read_pipe(self, *args, **kwargs):
        raise NotImplementedError()

    def reconnect(self, *args, **kwargs):
        raise NotImplementedError()

    def remove_logging_target(self, *args, **kwargs):
        raise NotImplementedError()

    def set_access_control(self, *args, **kwargs):
        raise NotImplementedError()

    def set_attribute_config(self, *args, **kwargs):
        raise NotImplementedError()

    def set_green_mode(self, *args, **kwargs):
        raise NotImplementedError()

    def set_logging_level(self, *args, **kwargs):
        raise NotImplementedError()

    def set_pipe_config(self, *args, **kwargs):
        raise NotImplementedError()

    def set_source(self, *args, **kwargs):
        raise NotImplementedError()

    def set_timeout_millis(self, *args, **kwargs):
        raise NotImplementedError()

    def set_transparency_reconnection(self, *args, **kwargs):
        raise NotImplementedError()

    def state(self, *args, **kwargs):
        raise NotImplementedError()

    def status(self, *args, **kwargs):
        raise NotImplementedError()

    def stop_poll_attribute(self, *args, **kwargs):
        raise NotImplementedError()

    def stop_poll_command(self, *args, **kwargs):
        raise NotImplementedError()

    def subscribe_event(self, *args, **kwargs):
        raise NotImplementedError()

    def unlock(self, *args, **kwargs):
        raise NotImplementedError()

    def unsubscribe_event(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attribute(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attribute_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attribute_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attributes(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attributes_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attributes_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def write_pipe(self, *args, **kwargs):
        raise NotImplementedError()

    def write_read_attribute(self, *args, **kwargs):
        raise NotImplementedError()

    def write_read_attributes(self, *args, **kwargs):
        raise NotImplementedError()


if __name__ == "__main__":
    ds = DeviceProxy("sys/tg_test/1")
    print(ds.read_attribute("ampli"))
    print(ds.read_attribute("bool_attr"))
    print(ds.read_attribute("float_attr"))
    print(ds.read_attribute("string_attr"))
    print("Read attribute configuration")
    print(ds.get_attribute_config("ampli"))
    print("Read attribute error")
    print(ds.read_attribute("error"))

