from math import modf


def get_digits(number, start, stop):
    # Shift all digit before start before "."
    number = number * 10 ** start
    # Remove decimal part
    number = modf(number)[0]
    # Shift untile stop
    number = number * 10 ** (stop - start)
    return int(number)

