# Mascot import
from pymascot.device_server_pb2_grpc import MascotDeviceServerServicer
from pymascot.device_server_pb2_grpc import (
    add_MascotDeviceServerServicer_to_server
)
from pymascot.error_pb2 import Error
from pymascot.attribute_config_pb2 import AttributeConfigReply
from pymascot.attributes_pb2 import ReadAttributeReply
from pymascot.commands_pb2 import CommandReply
from pymascot.tango_enums_pb2 import DevState, CmdArgType

# Grpc and run server
import grpc
from concurrent import futures

# local imports
from tango_enums import AttrQuality
from tango import Database

# Others
from time import time, sleep, strftime
from enum import Enum
from attribute import attribute
from utils import get_digits
import traceback
import sys


import logging

logging.basicConfig(level=logging.INFO)


class DeviceServicer(MascotDeviceServerServicer):
    """ The DeviceServer grpc service used to handle rpc request"""
    def __init__(self, servername):
        """ Setup a new Mascot server """
        self.servername = servername
        self.logger = logging.getLogger(servername)
        # Locals
        self.device_list = {}

    def add_device(self, name, device):
        """ Register a new device instance served by this server """
        self.logger.info("Register device {} : {}".format(device, name))
        self.device_list[name] = device

    def ReadAttribute(self, request, context):
        """ Perform a ReadAttribute grpc request """
        log = "Get read_attribute, request {} {}"
        self.logger.debug(log.format(request, context))
        # Unpack grpc request
        device_name = request.deviceName
        attribute = request.attributeName
        # Get the associated object.
        # TODO device not in the list
        device = self.device_list[device_name]
        try:
            # Perform a read request
            attr = device.read_attribute(attribute)
        except Exception:
            # Something wrong happen, logit
            self.logger.error(traceback.format_exc())
            # Build an Error answer
            error = Error(code=-1, message=traceback.format_exc())
            return ReadAttributeReply(success=False, error=error)
        return ReadAttributeReply(success=True, data=attr)

    def ReadAttributeConfig(self, request, context):
        """ Perform a ReadAttributeConfig grpc request """
        log = "Get read_attribute, request {} {}"
        self.logger.debug(log.format(request, context))
        # Unpack grpc request
        device_name = request.deviceName
        attribute = request.attributeName
        # Get the associated object.
        # TODO device not in the list
        device = self.device_list[device_name]
        try:
            # Perform a read request
            attr = device.read_attribute_config(attribute)
        except Exception:
            # Something wrong happen, logit
            self.logger.error(traceback.format_exc())
            # Build an Error answer
            error = Error(code=-1, message=traceback.format_exc())
            return AttributeConfigReply(success=False, error=error)
        return AttributeConfigReply(success=True, data=attr)

    def ExecuteCommand(self, request, context):
        log = "ExecuteCommand, request {} {}"
        self.logger.debug(log.format(request, context))
        device_name = request.deviceName
        command_name = request.commandName
        kwargs = request.parameters
        device = self.device_list[device_name]
        try:
            cmd = getattr(command_name, device)
            cmd_out =  cmd(**kwargs)
        except Exception:
            # Something wrong happen, logit
            self.logger.error(traceback.format_exc())
            # Build an Error answer
            error = Error(code=-1, message=traceback.format_exc())
            return CommandReply(success=False, error=error)
        return CommandReply(success=True, data=attr)





class DeviceMeta(type):
    """ Device Metaclass, maybe the attribute name and fget shall not be handle here  """
    def __new__(cls, name, bases, dct):
        for k, attr in dct.items():
            # Lets get all attributes and set there name
            if isinstance(attr, attribute):
                attr._name = k
                fget = attr._fget
                # Fget is not specified, let use the default one
                if not fget:
                    fget = "read_{}".format(k)
                attr._fget = fget
        return super().__new__(cls, name, bases, dct)


class Device(metaclass=DeviceMeta):
    """ Device class that can be insanciate by a mascot server """
    # Common attributes
    state = attribute(dtype=str, fget="dev_state")
    status = attribute(dtype=str, fget="dev_status")

    def __init__(self, name):
        self.__name = name
        # Setup logger
        self.logger = logging.getLogger(name)
        self.logger.info("Start device {}".format(name))
        # TODO, use DevState
        self.state.value = "UNKNOWN"
        self.status.value = "UNKNOWN"

    def init_device(self):
        pass

    def delete_device(self):
        pass

    def debug_stream(self, msg):
        self.logger.debug(msg)

    def info_stream(self, msg):
        self.logger.info(msg)

    def warn_stream(self, msg):
        self.logger.warn(msg)

    def error_stream(self, msg):
        self.logger.error(msg)

    def set_state(self, state):
        self.state.value = state

    def set_status(self, status):
        self.status.value = status
        self.__status = status

    def get_state(self):
        return self.state.value

    def get_status(self):
        return self.status.value

    def dev_status(self):
        return self.status.value

    def dev_state(self):
        return self.state.value

    def read_attr_hardware(self, attr_list):
        pass

    def always_executed_hook(self):
        pass

    def read_attribute(self, name):
        """ Read one attribute base on the attribute name"""
        # Get the mascot attribute
        attr = getattr(self, name)
        # Get the attribute read function
        attr_func = getattr(self, attr._fget)
        # Perform the read function
        values = attr_func()
        # Setup default
        value = None
        quality = AttrQuality.ATTR_VALID
        timestamp = time()
        # TODO What if value is a tupple len 2 or 3
        # Inspect the output of the read function
        if isinstance(values, tuple):
            # Only a value has been return
            if len(values) == 1:
                value = values[0]
            # 2 values, the first one is the read value, the second one
            # can be the quality or the timestamp
            elif len(values) == 2:
                value, other = values
                if isinstance(other, AttrQuality):
                    quality = other
                else:
                    timestamp = other
            # read function has return value quality and timestamp
            elif len(values) == 3:
                value, quality, timestamp = values
        else:
            # No tuple return, it is a single readvalue
            value = values
        # Update the attribute
        attr._value = value
        attr._quality = quality
        attr.set_timestamp(timestamp)
        # Build a reply
        return attr.build_attribute_reply()

    def read_attribute_config(self, name):
        attr = getattr(self, name)
        return attr.build_attribute_config_reply()

    def exec_command(self, name, kwargs):
        # TODO define command generator
        command = getattr(self, name)

    @classmethod
    def run_server(cls):
        run(cls)


def run(cls):
    # create a gRPC server
    try:
        server_name = sys.argv[1]
    except IndexError:
        print("A server name is required")
        return
    # Create grpc server
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    # Setup servicer
    device_service = DeviceServicer(server_name)
    # TODO: Querry database
    devices_name = ["sys/tg_test/1", "sys/tg_test/2"]
    # Instanciate mascot devices
    for name in devices_name:
        logging.info("class {}, device {}".format(cls, name))
        device_service.add_device(name, cls(name))

    # Server servicer
    add_MascotDeviceServerServicer_to_server(device_service, server)
    # listen on port 50051
    logging.info("Starting server. Listening on port 50001.")
    server.add_insecure_port("[::]:50001")
    server.start()
    # Export device:
    import os
    tango_host = os.getenv("TANGO_HOST")
    db = Database(tango_host)
    for name in devices_name:
        db.export_device(name, "lib-maxiv-mascot-py", "50001")
    logging.basicConfig(level=logging.INFO)
    try:
        while True:
            sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)




if __name__ == "__main__":
    class TestDevice(Device):
        ampli = attribute(dtype=int)
        bool_attr = attribute(dtype=bool)
        float_attr = attribute(dtype=float)
        string_attr = attribute(dtype=str)
        error = attribute(dtype=bool, fget="raise_error")

        def read_ampli(self):
            return 42

        def read_bool_attr(self):
            return True, AttrQuality.ATTR_INVALID

        def read_float_attr(self):
            return 34.5, 3550234237.123

        def read_string_attr(self):
            return (
                "Hello World",
                AttrQuality.ATTR_INVALID,
                2550234237.11111111111111111,
            )

        def write_ampli(self, value):
            print("Write ampli {}".format(value))

        def raise_error(self):
            raise ValueError("Some Error")


    run_server(TestDevice)

