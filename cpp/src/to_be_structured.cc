#include "to_be_structured.h"


WriteAttributeMessage mascot::DeviceAttribute_to_WriteAttributeMessage(const string& device_name, DeviceAttribute& da){
  WriteAttributeMessage wam;
  wam.set_attributename(da.name());
  wam.set_devicename(device_name);
  switch(da.value().value_case()) {
  case mascot::AttributeValue::kBoolValue : 
    wam.mutable_value()->set_boolvalue(da.value().boolvalue());
    break;
  case mascot::AttributeValue::kIntValue : 
    wam.mutable_value()->set_intvalue(da.value().intvalue());
    break;
  case mascot::AttributeValue::kDoubleValue : 
    wam.mutable_value()->set_doublevalue(da.value().doublevalue());
    break;
  case mascot::AttributeValue::kStringValue : 
    wam.mutable_value()->set_stringvalue(da.value().stringvalue());
    break;
  default :
    std::cout << "value type has yet to be implemented" << std::endl;
  }
  return wam;
}

//TODO
mascot::AttributeInfoEx mascot::AttributeConfig_to_AttributeInfoEx(AttributeConfig& ac) {
  AttributeInfoEx aie;
  aie.root_attr_name = ac.name(); 
  aie.memorized = (ac.memorized() ? mascot::AttrMemorizedType::MEMORIZED : mascot::AttrMemorizedType::NOT_KNOWN);
  aie.alarms = AttributeAlarmInfo();
  aie.enum_labels = vector<string>();
  aie.events = AttributeEventInfo();
  aie.sys_extensions = vector<string>();
  return aie;
}

std::ostream& operator<<(std::ostream& o, const mascot::DeviceAttribute& d){
  switch (d.value().value_case()) {
    case AttributeValue::ValueCase::VALUE_NOT_SET: 
      o << "The value doesn't exist";
      break;
    case AttributeValue::ValueCase::kBoolValue:    
      o << d.value().boolvalue();
      break;
    case AttributeValue::ValueCase::kIntValue:     
      o << d.value().intvalue();
      break;
    case AttributeValue::ValueCase::kDoubleValue:  
      o << d.value().doublevalue();
      break;
    case AttributeValue::ValueCase::kStringValue:  
      o << d.value().stringvalue();
      break;
    default: 
      o << "type has yet to been defined for printing";
      break;
  }
  return o;
}