#ifndef TO_BE_STRUCTURED_H
#define TO_BE_STRUCTURED_H

#include <string>
#include <vector>
#include <ostream>
#include "cppmascot/attributes.pb.h"
#include "cppmascot/attribute_config.pb.h"

using std::string;
using std::vector;
using mascot::AttributeValue;
using mascot::AttributeConfig;
using mascot::WriteAttributeMessage;
using mascot::DeviceAttribute;

std::ostream& operator<<(std::ostream& o, const mascot::DeviceAttribute& d);

namespace mascot {
//template <typename T>
//DeviceAttribute(const string& attr_name, const T& new_val) {
//    this->set_name()
//}

enum AttrMemorizedType {
    NOT_KNOWN = false, NONE = false, 
    MEMORIZED = true, MEMORIZED_WRITE_INIT = true
};

/*
enum DispLevel {
    OPERATOR = 0, EXPERT = 1
};

enum AttrDataFormat {
    SCALAR = 0, SPECTRUM = 1, IMAGE = 2, FMT_UNKNOWN = 3
};

enum AttrWriteType {
    READ = 0,
    READ_WITH_WRITE = 1,
    WRITE = 2,
    READ_WRITE = 3
};
*/

struct DeviceAttributeConfig {
    AttrDataFormat  data_format;
    int             data_type;
    string          description;
    string          display_unit;
    vector<string>  extensions;
    string          format;
    string          label;
    string          max_alarm;
    int             max_dim_x;
    int             max_dim_y;
    string          max_value;
    string          min_value;
    string          name;
    string          standard_unit;
    string          unit;
    AttrWriteType   writable;
    string          writable_attr_name;
};

struct AttributeInfo : DeviceAttributeConfig {
    DispLevel disp_level;
};

struct AttributeAlarmInfo {
    string          delta_t;
    string          delta_val;
    vector<string>  extensions;
    string          max_alarm;
    string          max_warning;
    string          min_alarm;
    string          min_warning;
};
struct AttributeEventInfo {/* NOT IMPLEMENTED */};
struct AttributeInfoList {/* NOT IMPLEMENTED */};
struct CommandInfoList {/* NOT IMPLEMENTED */};
struct DeviceInfo {/* NOT IMPLEMENTED */};
struct AttributeInfoEx : AttributeInfo {
    string              root_attr_name;
    AttrMemorizedType   memorized;
    AttributeAlarmInfo  alarms;
    vector<string>      enum_labels;
    AttributeEventInfo  events;
    vector<string>      sys_extensions;
};

AttributeInfoEx AttributeConfig_to_AttributeInfoEx(AttributeConfig&);
WriteAttributeMessage DeviceAttribute_to_WriteAttributeMessage(const string& device_name, DeviceAttribute&);
}

#endif