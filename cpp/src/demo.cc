#include <iostream>
#include "mascot.h"
using std::cout;
using std::endl;
using std::cin;

enum value_type {
  INT, DOUBLE, BOOL, STRING, UNKNOWN
};

value_type type_parser(string inp){
  if (inp == "int")     return INT;
  if (inp == "double")  return DOUBLE;
  if (inp == "bool")    return BOOL;
  if (inp == "string")  return STRING;
  return UNKNOWN;
}
void print_helpmsg(){
  cout << "------user guide:---------" << endl
       << "TWO COMMANDS AVAILABLE:"    << endl 
       << "read attribute"             << endl 
       << "write attribute type value" << endl;
}

int main(int argc, char** argv) {
  string name(argc > 1 ? argv[1] : "sys/tg_test/1");
  DeviceProxy dp(name);
  string cmd, attr, type, value;
  while(true){
    cin >> cmd >> attr;
    if (cmd == "read") {
      dp.read_attribute(attr);
    } else if (cmd == "write") {
      cin >> type >> value;
      DeviceAttribute da;
      da.set_name(attr);
      try {
      switch (type_parser(type)) {
        case INT : 
          da.mutable_value()->set_intvalue(std::stoi(value));
          break;
        case DOUBLE : 
          da.mutable_value()->set_doublevalue(std::stod(value));
          break;
        case BOOL : 
          da.mutable_value()->set_boolvalue(value=="true");
          break;
        case STRING : 
          da.mutable_value()->set_stringvalue(value);
          break;
        default :
          cout << "resorted to default" << endl;
      }
      dp.write_attribute(da);
      }catch (std::invalid_argument) {
        cout << "cant convert " << value << " to " << type << endl;
      }
    } else {
      print_helpmsg();
    }
    std::cin.ignore(INT_MAX, '\n');
  }
}
