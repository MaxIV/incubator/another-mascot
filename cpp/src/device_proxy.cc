#include "device_proxy.h"


using mascot::AttributeConfig_to_AttributeInfoEx;
using mascot::DeviceAttribute_to_WriteAttributeMessage;

GrpcClient::GrpcClient(const string& host_addr) {
  std::shared_ptr<Channel> channel = grpc::CreateChannel(host_addr, 
                                     grpc::InsecureChannelCredentials());
  proxy = MascotDeviceServer::NewStub(channel);
}

void GrpcClient::write_attribute(const string& device_name, DeviceAttribute & attr_in){
  ClientContext context;
  WriteAttributeMessage request(DeviceAttribute_to_WriteAttributeMessage(device_name, attr_in));
  WriteReply reply;
  proxy->WriteAttribute(&context, request, &reply);
  reply.success() ? std::cout << "write successful!"     << std::endl
                  : std::cout << reply.error().message() << std::endl;
}

DeviceAttribute GrpcClient::read_attribute(const string& device, const string& attr) {
  ClientContext context;
  ReadAttributeMessage message;
  ReadAttributeReply reply;
  message.set_devicename(device);
  message.set_attributename(attr);
  Status status = proxy->ReadAttribute(&context, message, &reply);
  reply.success() ? std::cout << reply.data()            << std::endl 
                  : std::cout << reply.error().message() << std::endl;
  return reply.data();
}

AttributeConfig GrpcClient::read_attribute_config(const string& device, const string& attr) {
  ClientContext context;
  ReadAttributeMessage message;
  AttributeConfigReply reply;
  message.set_devicename(device);
  message.set_attributename(attr);
  Status status = proxy->ReadAttributeConfig(&context, message, &reply);
  if(status.ok() || !reply.success()) {
    std::cout << attr << " attribute does not exist" << std::endl;
  }
  return reply.data();
}



DeviceProxy::DeviceProxy(const string& name) {
  this->name = name;
  server_address = string();
  if(get_address(name,server_address)) {
    std::cout << "Found registry" << std::endl;
    grpc_client = std::unique_ptr<GrpcClient> (new GrpcClient(server_address));
  } else { 
    std::cerr << "Could not find registry. Exiting..." << std::endl;
    std::exit(1);
  }
}

bool DeviceProxy::get_address(const string& name, string& address) {
  ClientContext context;
  DeviceName request;
  DirectoryImportReply reply;
  const char* db_address(std::getenv("TANGO_HOST"));
  if(!db_address) { 
    std::cout << "No TANGO_HOST env found. Trying " 
              << (db_address="lib-maxiv-mascot-py:50000") << std::endl;
  }
  auto db_connection = Directory::NewStub(grpc::CreateChannel(db_address, 
                               grpc::InsecureChannelCredentials()));
  request.set_name(name);
  context.set_deadline(std::chrono::system_clock::now() +
                       std::chrono::milliseconds(100));
  auto status = db_connection->ImportDevice(&context,request,&reply);
  if(status.ok()) address = reply.info().address();
  return status.ok();
}

void DeviceProxy::write_attribute(DeviceAttribute & attr_in) {
  grpc_client->write_attribute(name, attr_in);
}

DeviceAttribute DeviceProxy::read_attribute(const string& attr) {
  return grpc_client->read_attribute(name,attr);
}

AttributeInfoEx DeviceProxy::get_attribute_config(const string& attr) {
  AttributeConfig attribute_config = grpc_client->read_attribute_config(name,attr);
  return AttributeConfig_to_AttributeInfoEx(attribute_config);
}
