#ifndef DEVICE_PROXY_H
#define DEVICE_PROXY_H

#include <string>
#include <stdlib.h>
#include <grpcpp/grpcpp.h>
#include "mascot.h"
#include "cppmascot/device_server.grpc.pb.h"
#include "cppmascot/database.grpc.pb.h"
#include "cppmascot/attributes.pb.h"
#include <memory>
#include <iostream>
#include <vector>
#include <chrono>

using std::vector;
using std::string;
using grpc::ClientContext;
using grpc::Channel;
using grpc::Status;
using mascot::MascotDeviceServer;
using mascot::ReadAttributeMessage;
using mascot::ReadAttributeReply;
using mascot::AttributeConfigReply;
using mascot::Directory;
using mascot::AttributeConfig;
using mascot::DeviceAttribute;
using mascot::AttributeInfoEx;
using mascot::AttributeInfoList;
using mascot::WriteAttributeMessage;
using mascot::WriteReply;
using mascot::DeviceName;
using mascot::DirectoryImportReply;


class GrpcClient {
  public:
    GrpcClient(const string& host_addr);
    DeviceAttribute read_attribute(const string& device, const string& attr);
    AttributeConfig read_attribute_config(const string& device, const string& attr);
    void write_attribute(const string& device_name,DeviceAttribute & attr_in);
  private:
    std::unique_ptr<MascotDeviceServer::Stub> proxy;
};



class DeviceProxy {
  public:
    DeviceProxy(const string& name);
    DeviceAttribute read_attribute(const string& attr);
    AttributeInfoEx get_attribute_config(const string& attr);
    void write_attribute(DeviceAttribute & attr_in);
    AttributeInfoEx attribute_query(string att_name);/*{ return grpc_client.get_attribute_config(att_name); }*/
    AttributeInfoList *attribute_list_query();

    /**
     * Query the device for all commands information.
     *
     * Query the device for info on all commands. This method returns a vector of CommandInfo types. This
     * method allocates memory for the vector of CommandInfo returned to the caller. It is the caller responsibility
     * to delete this memory
     *
     * @return The command information list: One CommandInfo structure per command
     * @throws ConnectionFailed, CommunicationFailed, DevFailed from device
     */
    mascot::CommandInfoList *command_list_query();

    /**
     * Get device info.
     *
     * A method which returns information on the device in a DeviceInfo structure. Example :
     * \code
     * cout << " device info : " << endl
     * DeviceInfo dev_info = my_device->info() << endl;
     * cout << " dev_class " << dev_info.dev_class;
     * cout << " server_id " << dev_info.server_id;
     * cout << " server_host " << dev_info.server_host;
     * cout << " server_version " << dev_info.server_version;
     * cout << " doc_url " << dev_info.doc_url;
     * cout << " device_type " << dev_info.dev_type;
     * \endcode
     * All DeviceInfo fields are strings except server_version
     * which is a long integer.
     *
     * @return Device information structure
     *
     * @throws ConnectionFailed, CommunicationFailed, DevFailed from device
     */
    mascot::DeviceInfo const &info();

    /**
     * Write and read a single attribute
     *
     * Write then read a single attribute in a single network call. By default (serialisation by device), the execution
     * of this call in the server can’t be interrupted by other clients. To insert/extract the value to write/read you
     * have to use the operator of the class DeviceAttribute which corresponds to the data type of the attribute.
     * NOTE: There is no automatic type conversion from the user type to the attribute native type e.g. if an
     * attribute expects a short you cannot insert it as a double (this will throw an exception) you have to insert it
     * as a short.
     *
     * @param [in] attr_in Attribute name and value (to be written)
     * @return The read attribute data
     * @throws ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device
     */
    DeviceAttribute write_read_attribute(DeviceAttribute &attr_in);


  private:
    bool get_address(const string& name, string& address);
    string name, server_address;
    std::unique_ptr<GrpcClient> grpc_client;

};


#endif