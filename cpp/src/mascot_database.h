#ifndef MASCOT_DATABASE_H
#define MASCOT_DATABASE_H

#include <grpcpp/grpcpp.h>
#include "cppmascot/database.grpc.pb.h"
#include "cppmascot/database.pb.h"

#include <sstream>
#include <vector>
#include <string>
#include <memory>
#include <sqlite3.h>
using std::string;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;
using grpc::ClientContext;
using mascot::DbDevExportInfo;
using mascot::DbDevExportInfos;
using mascot::DirectoryReply;
using mascot::Directory;
using mascot::DeviceName;
using mascot::DirectoryImportReply;


#define DEFAULT_DATABASE "sqlite3/registry.db"

//swap to Directory::AsyncService later for asynchronous behaviour
class MascotDatabase final : public Directory::Service {
  public:
    explicit MascotDatabase();
    ~MascotDatabase();
    //export device imports device to registry
    Status ExportDevice(ClientContext* context, const DbDevExportInfo& request, DirectoryReply* response);
    Status ExportDevices(ClientContext* context, const DbDevExportInfos& request, DirectoryReply* response);
    //ImportDevice exports device to caller
    Status ImportDevice(ClientContext* context, const DeviceName& request, DirectoryImportReply* response);

  private:
    char *err_msg;
    sqlite3* db;
};

void RunServer();


#endif