#include "mascot_database.h"


MascotDatabase::MascotDatabase(){
  bool db_fail = sqlite3_open(DEFAULT_DATABASE,&db);
  if(db_fail) {
    std::cout << "Could not open database " << DEFAULT_DATABASE 
              << ", " << sqlite3_errmsg(db) << std::endl; 
    sqlite3_close(db);
    std::exit(EXIT_FAILURE);
  }
}

MascotDatabase::~MascotDatabase(){
  sqlite3_close(db);
}

Status MascotDatabase::ExportDevice(ClientContext* context, 
                                    const DbDevExportInfo& request, 
                                    DirectoryReply* response)
{
  std::ostringstream command;
  command << "INSERT INTO registry(name,address,version) VALUES("
          << request.name() << "," << request.address() << "," 
          << request.version() << ");";
  bool fd = sqlite3_exec(db,command.str().c_str(), 0, 0, &err_msg);
  if( fd != SQLITE_OK)
  {
    std::cerr << "Sql error: " << err_msg;
    response->set_success(false);
    sqlite3_free(err_msg);
    return Status::CANCELLED;
  }
  return Status::OK;

}

Status MascotDatabase::ExportDevices(ClientContext* context, 
                                     const DbDevExportInfos& request, 
                                     DirectoryReply* response)
{
  return Status::OK;
                                  
}
Status MascotDatabase::ImportDevice(ClientContext* context, 
                                    const DeviceName& request, 
                                    DirectoryImportReply* response)
{
  return Status::OK;
}


void RunServer() {
  std::string server_address("localhost:50000");
  MascotDatabase service;
  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;
  server->Wait();
}

int main(){
  RunServer();
}