DROP TABLE IF EXISTS registry;

CREATE TABLE registry
(
  name text primary key unique,
  address text,
  version text
)